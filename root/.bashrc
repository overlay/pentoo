# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


# Put your fun stuff here.

#if [ ! "$(grep nox /proc/cmdline)" ]; then
#	if [ -x /usr/bin/X ]; then
#		if [ -e /etc/startx -a $(tty) = "/dev/tty1" ]; then
#			#rm -f /etc/startx
#			echo startx | su - 'pentoo'
#			[ -f /etc/motd ] && cat /etc/motd
#		fi
#	fi
#fi

# fecha
alias fecha1='date "+%d/%m/%Y %H:%M:%S"'
alias fecha2='date "+%d%m%Y_%H%M%S"'
alias fecha3='date +%D | sed -e "s|/||g"'

export EDITOR=vim
export PAGER=more

# backup file
backup(){
cp -v "$1"{,."$(date "+%d%m%Y_%H%M%S")"}
}

